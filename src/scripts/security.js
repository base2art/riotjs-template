var babaBuiSecurity = (function () {


    return {
        namespace: "security",
        scope: function (wndw, doc, application) {
            
            application.verifyModule("io");
            application.verifyModule("net");
            
            const security = {};
            let __knownRoles = {};
            security.knownRoles = function () {
                return Object.keys(__knownRoles).map(x=>{ return {name:x, requireAuthenticated:true};});
            };

            security.registerRole = function(name) {
                __knownRoles[name] = true;
            };

            let authenticator = {
                id: function(){
                    return "";
                },
                isAuthenticated: function() {
                    return false;
                },
                hasRole: function(role) {
                    return false;
                },
                canSignIn: function(){
                    return false;
                },
                signIn: function(){

                }
            };
            security.setAuthenticator = function(registrar) {
                authenticator = registrar;
            };

            security.authenticators = {};
            security.authenticators.default = function(userName, roles) {
                let userName1 = userName;
                let roles1 = roles || [];
                return {
                    id: function(){
                        return userName1;
                    },
                    isAuthenticated: function() {
                        return userName1 != null && userName1.length >= 0;
                    },
                    hasRole: function(role) {
                        return roles1.map(x=>x.toUpperCase()).includes(role.toUpperCase());
                    },
                    canSignIn: function(){
                        return true;
                    },
                    signIn: function(){

                    }
                };
            };

            const user = {};
            security.user = user;


            user.canSignIn = function () {
                authenticator.canSignIn();
            };

            user.signIn = function () {
                authenticator.signIn();
            };

            user.id = function () {
                return authenticator.id();
            };

            user.isAuthenticated = function () {
                return authenticator.isAuthenticated();
            };

            user.isAuthorized = function (role) {

                if (role == null || role.length === 0) {
                    return true;
                }
                
                security.registerRole(role);

                let roles = security.knownRoles().filter(x => x.name === role);
                
                let isAuthenticated = user.isAuthenticated();

                for (let i = 0; i < roles.length; i++) {
                    let targetRole = roles[i];
                    if (targetRole.requireAuthenticated && !isAuthenticated) {
                        return false;
                    }

                    if (!authenticator.hasRole(targetRole.name))
                    {
                        return false;
                    }
                }

                return true;
            };
            
            return security;
        }
    };
})();
