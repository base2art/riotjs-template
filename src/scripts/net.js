var babaBuiNet = (function () {


    return {
        namespace: "net",
        scope: function (wndw, doc, application) {


            let net = {};
            net.getParameterByName = function (name, url) {
                if (!url) {
                    url = wndw.location.href;
                }
                name = name.replace(/[\[\]]/g, "\\$&");
                let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            };
            net.getCookie = function (cname) {
                let name = cname + "=";
                let ca = doc.cookie.split(';');
                for (let i = 0; i < ca.length; i++) {
                    let c = ca[i];
                    while (c.charAt(0) === ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) === 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                
                return "";
            };
            net.primaryDomain = function () {
                return wndw.location.host.split('.').slice(-2).join(".");
            };

            return net;
        }
    };

})();



