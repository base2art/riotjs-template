var babaBuiIO = (function () {


    const io = {};
    io.clone = function jsonCopy(src) {
        return JSON.parse(JSON.stringify(src));
    };

    return {
        namespace: "io",
        scope: function (wndw, doc, application) {
            return io;
        }
    };

})();



