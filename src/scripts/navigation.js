var babaBuiNavigation = (function () {


    return {
        namespace: "navigation",
        scope: function (wndw, doc, application) {

            application.verifyModule("io");
            application.verifyModule("security");

            let handlers = [];
            let navigation = {

            };

            let router = {};
            let globalPanels = {panels:[]};
            navigation.navigated = function () {
                for (let i = 0; i < handlers.length; i++) {
                    handlers[i]();
                }
            };

            navigation.registerNavigatedHandler = function (item) {
                handlers.push(item);
            };


            let panelBuilder = function (urlBuilder, urlObj, panelObject) {


                let panelObject1 = panelObject;
                let urlObj1 = urlObj;
                let ret = {};
                let urlBuilder1 = urlBuilder;

                ret.requireRole = function (role) {
                    panelObject1.requiredRole = role;
                    return ret;
                };

                ret.titled = function (title) {
                    panelObject1.title = title;
                    return ret;
                };

                ret.refresh = function (interval) {
                    panelObject1.refreshInterval = interval;
                    return ret;
                };

                ret.modelProvider = function (call) {
                    panelObject1.dependencyCall = call;
                    return ret;
                };

                ret.add = function () {
                    urlObj1.panels.push(panelObject1);
                    let security = application.module("security");
                    if (panelObject1.requiredRole) {
                        security.registerRole(panelObject1.requiredRole);
                    }

                    return urlBuilder1;
                };

                return ret;
            };


            let routeBuilder = function (backingObject) {

                let backingObject1 = backingObject;
                let ret = {};

                ret.panel = function (position, tagName) {
                    let panelObj = {
                        position: position,
                        tagName: tagName
                    };

                    return panelBuilder(ret, backingObject1, panelObj);

                };

                return ret;
            };


            navigation.panel = function (position, tagName) {
                let panelObj = {
                    position: position,
                    tagName: tagName
                };

                return panelBuilder(navigation, globalPanels, panelObj);

            };

            navigation.addRoute = function (title, url, parameters, requiredRole, includeInNav) {
                let urlObj = {
                    url: url,
                    title: title,
                    parameters: parameters,
                    requiredRole: requiredRole,
                    panels: [],
                    includeInNav: ((includeInNav == null) ? ((parameters || []).length === 0) : includeInNav)
                };

                let security = application.module("security");
                if (requiredRole) {
                    security.registerRole(requiredRole);
                }

                router[url] = urlObj;
                return routeBuilder(urlObj);
            };

            navigation.currentPage = {
                title: "Unknown",
                url: "/",
                urlForDisplay: "/",
                parameters: {},
                panels: []
            };

            navigation.routes = function () {
                let keys = Object.keys(router);
                return keys.map(key => router[key]);
            };

            navigation.navigate = function (url, params) {
                let page = router[url];

                if (page == null) {
                    console.log("cannot navigate to ", url, params);
                    return "";
                }


                let navigationParameters = Object.keys(params);

                if (!page.parameters.every(val => navigationParameters.includes(val))) {
                    console.log("cannot navigate to ", url, " because missing parameters for page", params);
                    return "";
                }

                let qsUrl = url;
                for (let i = 0; i < page.parameters.length; i++) {
                    let pageParameter = page.parameters[i];

                    qsUrl = qsUrl.replace("{" + pageParameter + "}", params[pageParameter]);
                }

                navigation.currentPage.url = url;
                navigation.currentPage.urlForDisplay = qsUrl;
                navigation.currentPage.title = page.title;
                navigation.currentPage.parameters = params;
                let myPanels = page.panels || [];

                navigation.currentPage.panels = globalPanels.panels.concat(myPanels);
                // navigation.currentPage.panels.forEach(x=>x.skipBinding = navigation.skipBinding);


                let role = page.requiredRole;
                if (!application.security.user.isAuthorized(role)) {
                    console.log("cannot navigate to ", url, " because you are not authorized", params);
                    navigation.routeToUrl("/");
                    return "";
                }

                console.log("navigating", qsUrl, params);
                history.pushState(null, null, "#" + qsUrl);
                navigation.navigated();
            };


            navigation.routeToUrl = function (url) {
                if (url != null) {
                    let normalizeParts = function (item) {
                        while (item.includes("//")) {
                            item = item.replace("//", "/");
                        }

                        return item.split('/').filter(x => x != null && x.length > 0
                        )
                            .map(function (x) {
                                return {
                                    mask: ((x.startsWith("{") && x.endsWith("}")) ? "----------------------" : x),
                                    value: x
                                };
                            });
                    };

                    function isMatch(hashParts, urlParts) {
                        if (urlParts.length !== hashParts.length) {
                            return [false, {}];
                        }

                        let outParams = {};

                        for (let up = 0; up < urlParts.length; up++) {
                            let urlPart = urlParts[up];
                            let hashPart = hashParts[up];

                            if (urlPart.value !== hashPart.value && urlPart.value === urlPart.mask) {
                                return [false, {}];
                            }

                            if (urlPart.value !== urlPart.mask) {
                                let cleanPart = urlPart.value.substr(1, urlPart.value.length - 2);
                                outParams[cleanPart] = hashPart.value;
                            }
                        }


                        return [true, outParams];
                    }

                    function getMatch(hashParts, routes) {

                        for (let u = 0; u < routes.length; u++) {
                            let route = routes[u];

                            let urlParts = normalizeParts(route.url);

                            let match = isMatch(hashParts, urlParts);
                            if (match[0]) {
                                return [route, match[1]];
                            }
                        }
                    }

                    let hashParts = normalizeParts(url);

                    let routes = navigation.routes();
                    let route = getMatch(hashParts, routes);

                    if (route != null) {
                        navigation.navigate(route[0].url, route[1]);
                        return;
                    }
                }

                let keys = Object.keys(router);
                let route = keys.map(key => router[key]).filter(x => x.includeInNav === true)[0];
                navigation.navigate(route.url, {});
            };

            return navigation;
        }
    };
})();

