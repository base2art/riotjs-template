var babaBuiWebClient = (function () {


    return {
        namespace: "webClient",
        scope: function (wndw, doc, application) {

            let app = application;
            let webClient = {};

            var builder = function (obj) {
                var data = obj;

                var builderObj = {};

                builderObj.withBody = function (body) {

                    data["body"] = body;
                    return builderObj;
                };


                builderObj.do = function (callback) {

                    app.workingState.setIsBusy(true);
                    const headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    const options = {
                        method: data.method,
                        headers: {
                            'Content-type': 'application/json; charset=UTF-8'
                        }
                    };

                    if (data.body) {

                        options.body = JSON.stringify(data.body);
                    }

                    fetch(data.url, options)
                        .then(response => {
                            app.workingState.setIsBusy(false);
							if (callback == null) {
                                return null;
                            }

							try
							{
                                return response.json();	
							}
							catch (ex)
							{
								return null;
							}
                        })
                        .then(body => {
                            if (callback) {
                                callback(body);
                            }
                        });

                };

                return builderObj;
            };

            webClient.rest = function (method, url, parameters) {

                if  (parameters) {
                    let keys = Object.keys(parameters);
                    
                    for (let i=0; i<keys.length; i++) {
                        let key = keys[i];
                        let value = parameters[key];
                        url = url.replace("{" + key + "}", encodeURIComponent(value));
                    } 
                }
                
                return builder({url: url, method: method});
            };

            return webClient;
        }
    };

})();



