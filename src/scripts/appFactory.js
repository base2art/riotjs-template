var babaBuiAppFactory = (function (wndw, doc) {

    let myWindow = wndw;
    let myDoc = doc;

    let registry = [];

    return {

        registerModule: function (obj) {
            registry.push(obj);
        },
        createApplication: function () {


            const root = {
                verifyModule: function (name) {
                    for (let i = 0; i < registry.length; i++) {
                        let r = registry[i];
                        if (r.namespace === name) {
                            return;
                        }
                    }

                    throw "Module: '" + name + "' Not Found;";
                }
            };

            root.module = function (name) {
                return root[name];
            };

            root.workingState = {};
            root.workingState.isBusy = false;
            root.workingState.counter = 0;


            let guidFormatter = {};
            guidFormatter["D"] = (a1, a2, a3, a4, a5, a6, a7, a8) =>
                a1 + a2 + '-' + a3 + '-' + a4 + '-' + a5 + '-' + a6 + a7 + a8;

            guidFormatter["N"] = (a1, a2, a3, a4, a5, a6, a7, a8) =>
                a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8;

            guidFormatter["B"] = (a1, a2, a3, a4, a5, a6, a7, a8) =>
                "{" + guidFormatter.D(a1, a2, a3, a4, a5, a6, a7, a8) + "}";

            guidFormatter["P"] = (a1, a2, a3, a4, a5, a6, a7, a8) =>
                "(" + guidFormatter.D(a1, a2, a3, a4, a5, a6, a7, a8) + ")";


            root.newUuid = function (format) {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }

                format = format || "D";

                return guidFormatter[format](s4(), s4(), s4(), s4(), s4(), s4(), s4(), s4());
            };

            let root_workingState_handlers = {items: []};

            root.workingState.setIsBusy = function (value) {

                root.workingState.counter += (value ? 1 : -1);


                root.workingState.isBusy = root.workingState.counter > 0;
                let localHandlers = root_workingState_handlers.items;
                for (let i = 0; i < localHandlers.length; i++) {
                    let localHandler = localHandlers[i];
                    if (localHandler.enabled) {
                        localHandler.func();    
                    }
                }
            };

            root.workingState.registerHandler = function (value) {

                var localHandlers = root_workingState_handlers.items;
                let id = root.newUuid("N");
                localHandlers.push({func: value, id: id, enabled: true});
                return id;
            };

            root.workingState.clearHandler = function (id) {
                root_workingState_handlers.items = root_workingState_handlers.items.filter(x => x.id !== id);
            };

            for (let i = 0; i < registry.length; i++) {
                let r = registry[i];
                if (!(r.namespace in root)) {
                    root[r.namespace] = r.scope(myWindow, myDoc, root);
                }
            }

            root.actions = {
                alert: function (x) {
                    alert(JSON.stringify(x, null, "  "));
                }
            };

            root.addAction = function (name, func) {
                root.actions[name] = func;
            };

            return root;
        }

    };

})(window, document);

